/**
 * @file
 * Main javascript file for Bingo card.
 */
document.addEventListener('DOMContentLoaded', () => {
  /**
   * Creates a bingo card from tables with specified markup.
   */
  class BingoCard {
    constructor(element) {
      // Set up the variables we need for the card object.
      this.selectors = {
        checkedDexter: null,
        checkedSinister: null,
        freeSquare: '.awp__bingo-card-cell--free',
        number: '.awp__bingo-card-cell-number',
        row: '.awp__bingo-card-row',
        square: '.awp__bingo-card-cell',
        state: '.awp__bingo-card-cell-state',
      };
      this.card = element;
      this.numbers = this.card.querySelectorAll(this.selectors.number);
      this.states = this.card.querySelectorAll(this.selectors.state);
      this.freeSquare = this.card.querySelector(this.selectors.freeSquare);

      // "Randomize" the squares.
      this.shuffleSquares();

      // Mark the free square.
      this.setFreeSquare();

      // Determines selectors for diagonal bingo check.
      this.getDiagonalSelectors();

      // Sets up event listeners on bingo squares.
      this.states.forEach((el) => {
        el.addEventListener('change', this.markCard.bind(this));
      });
    }

    /**
     * Creates selector lists for "bend dexter" and "bend sinister".
     *
     * (In heraldry, "bend dexter" is a diagonal like going from B0 to 05 on a
     * bingo card, and "bend sinister" is a diagonal like going from B5 to 01
     * on a bingo card).
     */
    getDiagonalSelectors() {
      // Set variables.
      const selectorDexter = [];
      const selectorSinister = [];

      // Loop over the letters 'b', 'i', 'n', 'g', and 'o'. Since our card has
      // five columns AND five rows, we can use forEach()'s iterator variable
      // to determine the current row.
      'bingo'.split('').forEach((el, i) => {
        selectorDexter.push(`#${el}-${i + 1}:checked`);
        selectorSinister.push(`#${el}-${5 - i}:checked`);
      });

      this.selectors.dexter = selectorDexter.join(', ');
      this.selectors.sinister = selectorSinister.join(', ');
    }

    /**
     * Checks for bingo in clicked cell's row & column, or any diagonal bingo.
     */
    markCard(ev) {
      // Find row and column of clicked.
      const xy = ev.target.id.split('-');
      // There are five rows and five columns, so we're creating selectors for
      // the needed cells on the spot.
      const xInRow = this.card.querySelectorAll(`${this.selectors.row}--${xy[1]} :checked`);
      const xInCol = this.card.querySelectorAll(`${this.selectors.square}--${xy[0]} :checked`);
      // There are only two diagonals though, so we've pre-built their
      // selectors in the constructor.
      const xInDexter = this.card.querySelectorAll(this.selectors.dexter);
      const xInSinister = this.card.querySelectorAll(this.selectors.sinister);

      // Mark cells that belong to a series of five horizontal, vertical, or
      // diagonal checked squares.
      xInRow.forEach((el) => { el.dataset.rowBingo = (xInRow.length === 5); });
      xInCol.forEach((el) => { el.dataset.colBingo = (xInCol.length === 5); });
      xInDexter.forEach((el) => { el.dataset.dexterBingo = (xInDexter.length === 5); });
      xInSinister.forEach((el) => { el.dataset.sinisterBingo = (xInSinister.length === 5); });
    }

    /**
     * Replaces the text of the central square.
     *
     * Handling this in a separate function allows us to leave the shuffle()
     * function completely stock, and to NOT include the Free cell in the basic
     * table markup (which in turn lets us include only 24/25 possible cell
     * values, making it very slightly harder to win).
     */
    setFreeSquare() {
      this.freeSquare.querySelector(this.selectors.number).textContent = 'Free';
    }

    /**
     * Uses Fisher-Yates shuffle to "randomize" the order of bingo card squares.
     *
     * @see https://bost.ocks.org/mike/shuffle/
     */
    shuffleSquares() {
      let { length } = this.numbers;

      while (length) {
        const random = Math.floor(Math.random() * length--);

        [this.numbers[length].textContent, this.numbers[random].textContent] = [
          this.numbers[random].textContent, this.numbers[length].textContent,
        ];
      }
    }
  }

  const cardSelector = '.awp__bingo-card';

  const cardTable = document.querySelector(cardSelector);

  new BingoCard(cardTable, cardSelector);
});
